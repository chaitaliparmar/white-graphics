/**
 * scripts.js
 * Contains Script for basic static website named "White Graphics"
 */


$(window).on('load',function(){
	$("#preloader").delay(500).fadeOut('slow');
});

$(document).ready(function(){
	$("#team-right").owlCarousel({
		items:2,
		autoplay:true,
		margin:20,
		loop:true,
		nav:true,
		smartspeed:700,
		autoplayHoverPause:true,
		dots:false,
		navText:['<i class="lni-chevron-left-circle"></i>','<i class="lni-chevron-right-circle"></i>']
	});
});

$(document).ready(function(){
	$("#clients").owlCarousel({
		items:6,
		autoplay:true,
		margin:20,
		loop:true,
		nav:true,
		smartspeed:700,
		autoplayHoverPause:true,
		dots:false,
		navText:['<i class="lni-chevron-left-circle"></i>','<i class="lni-chevron-right-circle"></i>'],
		responsive:{
		0:{
			item:2,
		},
		480:{
			item:3,
		}
	}
	});
});


$(document).ready(function(){
	$("#cards").owlCarousel({
		items:1,
		autoplay:true,
		margin:20,
		loop:true,
		nav:true,
		smartspeed:700,
		autoplayHoverPause:true,
		dots:false,
		navText:['<i class="lni-chevron-left-circle"></i>','<i class="lni-chevron-right-circle"></i>']
	});
});
$(document).ready(function(){
	$("#progress-elements").waypoint(function(){
		$(".progress-bar").each(function(){
			$(this).animate({
				width:$(this).attr("aria-valuenow")+"%"
			},800);
		});
		this.destroy();
	},{
		offset:'bottom-in-view'
	});
});
$(document).ready(function(){
	$('#services-tabs').responsiveTabs({
		animation:'slide'
		
	});
});
/****************************************************
					PORTFOLIO SECTION 
***************************************************/
$(document).ready(function(){
	$("#isotope-container").isotope({});
	
	$("#isotope-filters").on("click","button",function(){
		let filterValue = $(this).attr("data-filter");
		$("#isotope-container").isotope({
			filter: filterValue
		});
		$("#isotope-filters").find('.active').removeClass('active');
		$(this).addClass('active');
	});
});

$(document).ready(function(){
	$("#portfolio-wrapper").magnificPopup({
		delegate:'a',
		type:'image',
		gallery:{
			enabled:true	
		},
		zoom:{
			enabled:true,
			duration:300,
			easing:'ease-in-out',
			opener:function(openerElement){
				return openerElement.is('img') ? openerElement:  openerElement.find('img');
			}
		}
	})
});

$(document).ready(function(){
	$('.counter').counterUp({
		delay:10,
		time:1000
	})
});

$(window).on('load',function(){
	var addressString ="301,Evergreen CHS.,Airoli,Maharashtra,India";
	var myLatLng = {
		lat: 19.173829,
		lng: 72.953716
	};
	var myMap = new google.maps.Map(document.getElementById("map"),{
        zoom:13,
        center:myLatLng
    });
    
     var custimg = "img/logo/";
    var marker = new google.maps.Marker({
        position:myLatLng,
        map:myMap,
        draggable: true,
        animation: google.maps.Animation.BOUNCE,
        title:"click to see",
        icon: custimg + 'SLlogo.PNG',
    });
    
	var image = new google.maps.MarkerImage(
    place.icon,
    new google.maps.Size(71, 71),
    new google.maps.Point(0, 0),
    new google.maps.Point(17, 34),
    new google.maps.Size(25, 25));
//    var image =
	var image = {
  url: place.icon,
  size: new google.maps.Size(71, 71),
  origin: new google.maps.Point(0, 0),
  anchor: new google.maps.Point(17, 34),
  scaledSize: new google.maps.Size(25, 25)
};
    
    var infoWindow = new google.maps.InfoWindow({
       content:addressString 
    });
    marker.addListener('click',function(){
        infoWindow.open(myMap,marker);
    });
    
});
new WOW().init();
